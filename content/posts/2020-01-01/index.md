---
title: Google Chrome Is Getting a New Machine Learning Model That Groups Its Users 🍪👁🧮
author: Ramiro Romani
date: 2021-02-11
hero: ./images/hero.jpg
excerpt: The Chromium Project just released details on how user tracking may look in the future.
---


Read exclusive content at [@neo_network](https://t.me/neo_network)
## THE NEWS 

The Chromium Project, the open source Google-funded initiative that developed the engine behind Google Chrome and many other browsers (Brave, Opera, Microsoft Edge) released new details on their 2019 proposal for [The Privacy Sandbox] (https://www.chromium.org/Home/chromium-privacy/privacy-sandbox).
Given Chrome's 65% market share across all browsers - this is going to be huge. 

## WHATS THE PROPOSAL? 
Chromium is starting to get rid of third-party cookie tracking and fingerprinting techniques which are traditionally used to target relevant advertisements to you as you surf the web (and implicitly track you). They are in favor of replacing it with new functionality that aims to have the same result of advertisers and users. It was hard to get any details on this functionality until late last year, and now more things are being explained.

## WHATS THE NEW FUNCTIONALITY? 
New details were announced last month about a [Federated Learning Of Cohorts] (https://github.com/WICG/floc) which is one of the highly favored proposals and will be released for testing in March (among others). It works by feeding a user's browsing behavior to a local Machine Learning model that groups users into clusters, which can then be used to target ads specifically to these groups.  

Behind this functionality is a new browser API, which is lcoal to your machine and gets updated with new information as you browse the internet. The machine learning model may be fed 'URLs, content, or other factors' to cluster you. Its important to note that your browsing data isn't exposed, just your cohort, which the browser ensures is 'well-distributed', each cohort may represent thousands of people. 

BENEFITS
So does this improve privacy? It will in the sense of the large ad networks that use third party cookies to create targeted advertising profiles of their users. But browsers like Firefox, Safari, Brave, and Edge have already created measures to block third party cookies, and there are plenty of addons that can help with this, although they aren't used by many people. 

RISKS
There are a few risks that the proposal recognizes itself. The first that users can now be tied to their general interests (or theoretically, any cluster that a cohort wants to group them in) through a site that has their PII or email. The cohorts can also be used as a pseudo-identifier, and be used to identify someine in addition with their IP address, if the cohort sizes are small enough. Lastly, a cohort might reveal sensitive information about the user. Such as someone who works in a particular industry, an investigative journalist, people who browse taboo content.

WHAT CAN WE DO? 
If you're a Chromium user, just be prepared for the rollout of the new Privacy Sandbox features in the coming months, with origin tests underway and advertisers tests in Q2. Google plans to release opt-out functionality for these new features sometimes in April.

If you're a web developer, you can opt out of cohort computation using a permissions policy, which for cohorts will be allowed by default. This will exclude the url and content of that site from being used in the cohort calculation. 

How do you feel about these new web features? 
Are they an improvement to your privacy? 
Could they be abused to put users into groups? 

Discuss with the [neo_network](https://t.me/neo_network_chat)
