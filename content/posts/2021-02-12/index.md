---
title: The War Against Alternative Search Engine's (Gigablast)
author: Ramiro Romani
date: 2021-02-12
hero: ./images/hero.jpg
excerpt: Are big search engines colluding with social media and governments? 
---


#### Did you know that there was an independent search engine launched only a few years after Google was founded? In 2000, Matt Wells created Gigablast to index hundreds of billions of pages with the least amount of hardware possible. 

Read exclusive content at [@neo_network](https://t.me/neo_network)

[Gigablast](https://www.gigablast.com/), the subject of today's piece was brought to my attention by a reader, if you've got cool stuff to share, let me know at: 
https://t.me/neo_network_chat

Before beginning this piece, you should know how a search engine works. Search engines use web crawlers (or spiders), which are bots that navigate the web and download web pages, using links to discover new webpages, and save the content to an index along with keywords and content types. The index is what you're actually searching through when you use a search engine. 

If you read through Gigablast's blog, you'll see that he's been fighting unfair treatment by the likes of tech-giants since 2003 - like Versign (which is an authoritative domain registry, and runs 2 of the 13 Internet's root namerservers). 

More recently, he's been having to fight the collusion & anti-competitive practices of tech giants trying to keep out smaller or newer search engines. 

Here are some key points:
- Google demands exclusivity from any company that displays Google results (google results cannot be mixed with other search engine's), destroying any meta-search engines (search engines that combine results from multiple places)
- Cloudfare CDN (if you've ever seen those DDOS protection warnings) which is heavily funded by Google, Bing, and Baidu, interferes with the indexing of context from millions of PUBLIC websites under the guise of 'protection' so smaller search engines cannot build their results. Cloudfare even allows the Chinese search engine Baidu to have their results unimpeded. Are you aware how big this monopoly really is?  
-  Github, Youtube, Facebook & LinkedIn are no longer openly shared platforms, they only share data with Google & Bing but limit or outright disallow other search engines. Quit using LinkedIn and Facebook. 
-  The US Government seems to support the Google monopoly (as if it was a suprise), limiting the rate at which alternative search engines can index the content of US government sites, while not imposing the same restrictions on Google. 

Its obvious that all content on the web is being monopolized in the hands of a few, our sources of information are being dwindled and centralized in the hands of a few, which make it easier to censor, and kill any independent alternatives. 

What can we do?! 

If you use the web:
- Use alternative search engines as much as possible like Gigablast, and the new private search engine private.sh which uses Gigablast as a search provider and offers encrypted searches (where the contents of your query are encrypted so only Gigablast can read it)

If you're a creator:
- Get off Youtube & Facebook, there are platforms like Odysee and Minds which are independent and strive to be open 
- Get off LinkedIn which is harvesting & monetizing your open data 
- Do not use Cloudfare for DDOS protection, your site will not be indexed by any alternative search engines, and thus - if your site ever breaks the 'rules', no one will be able to find it. 

Were you aware of the serach engine collusion between Microsoft, Google, Cloudfare, and Chinese companies like Baidu? How does this change your stance on them? 

Are Youtube's, LinkedIn, and Github's practices of blocking alternative search engines inherently anti-competitive? What impact does this have on the internet? 

Discuss with the [neo_network](https://t.me/neo_network_chat)
