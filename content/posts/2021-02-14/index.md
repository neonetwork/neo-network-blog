---
title: The Third Iteration Of The Internet Part I, IPFS
author: Ramiro Romani
date: 2021-02-14
hero: ./images/IPFS-Large.jpg
excerpt: The internet is changing in fundamental ways, and IFPS one of the new technologies driving it...
---

# The Third Iteration Of The Internet: Part I, IPFS 🍪👁🧮
[@neo_network](https://t.me/neo_network)


The previous & somewhat current generation of the internet, that started in the early 2000's and is now fully matured is called Web 2.0.

This generation of the web gave rise to:
- Interactivity & user participation, dynamic content, electronic economies
- Data & power in the hands of a few, through huge centralized data stores (search engines, social media platforms)
- Advertisements & monetization, and its pressure on web content to keep people engaged 
- Censorship by blocking access to singular servers 

We're now seeing a movement towards a decentralized web, and in this series of posts we'll learn about some of the leading technologies. 

Today, we'll talk about IPFS, Inter-Planetery-File-Sytem. 

IPFS is a internet protocol and peer-to-peer storage network released in 2015 by Protocol Labs. IPFS has already been used as the file storage for several revolutionary projects like Brave and OpenBazaar. It has also been used to circumvent internet censorship, such as when [Wikipedia was blocked in Turkey.](https://turkeyblocks.org/2017/04/29/wikipedia-blocked-turkey/) 

IPFS flips the philosophical paradigm of content ownership and access. Instead of asking the file's owners for access to content (like you do today when hitting a website), you ask to have content made available from a network of computers who possess each other's files. 

This is similar to the peer-to-peer torrenting software BitTorrent,  which helped rapidly distribute music, movies, and software to the masses. 

When using IPFS, your computer is an active participant in the network, making downloaded files available to others who may want them. 

Another major principle of IPFS is its verifiability, meaning you can be sure that a piece of content is a genuine copy of the original. IPFS does through content addressing, which is a unique content identifier created by hashing the content of the file over and over until its a manageable string. This identifier will dramatically change when files are changed. 

So how is content actually stored on IPFS? Content is split up into blocks, and related to each other in a DAG (Directed Acyclic Graph, also used in Git's version control software), where each node has a content identifier that is the hash of its contents (its children). Breaking up files into blocks enable it to be downloaded from different sources and rebuilt together. (damn if only Humpty Dumpty was around to see this) 

And lastly, how the hell do you ask for content from an entire network at once? 

When asking for a specific file from the network, you'll have to look it up in a Distributed Hash Table (give me all the acronyms), which is like a dictionary of names & addresses, but distributed among many computers in a network. 

There's two parts to this: 
1. Lookup the names of the nearest computers that can serve up the content
2. Find the current location of those computers 

Then, its just a matter of connecting to those computers and requesting the blocks that you need.

Boom! Simple, right? 

What should you do next? 

1. Try navigating the DWeb. 

    You can navigate to the DWeb through an [ HTTP gateway](https://ipfs.github.io/public-gateway-checker/) by using a gateway host followed by an IPFS address. 

    The address will look something like: 
    https://ipfs.io/ipfs/bafybeifx7yeb55armcsxwwitkymga5xf53dxiarykms3ygqic223w5sk3m#x-ipfs-companion-no-redirect

2. Become a part of the DWeb, run [IPFS](https://github.com/ipfs-shipyard/ipfs-desktop#ipfs-desktop) on your computer. 

Questions For The Reader

1. What are the effects of the DWeb on censorship, monetization, and user tracking? 

2. How fast do you see the DWeb taking off? 

[Discuss in our Telegram Group](https://t.me/neo_network_chat)

That was a lot of learning. I hope this writeup was informative. As always, join the active discussion at [@neo_network](https://t.me/neo_network), and I'll see on the decentralized web. 