---
title: Part I - ProtonMail is Inherently Insecure, Your Emails Are Likely Compromised, Ties To Government Agencies
author: Ramiro Romani
date: 2021-02-15
hero: ./images/data-center.jpg
excerpt: Do you know everything about the popular Swiss based 'private' email provider? Check this out...
---

# ProtonMail is Inherently Insecure, Your Emails Are Likely Compromised, Ties To Government Agencies: Part I ⚛️❗️🔓
[@neo_network](https://t.me/neo_network)

I'm currently writing this post in a dark room by candelight and it seems fitting. 

Ever since I got into this space, and even before, people have always been quick to recommend ProtonMail, a 'private' email service based in Switzerland.  

I'll admit though, I went along with it and used the service, but after a while, seeing its growth gave me an uneasy gut feeling. 

As I should have done half a year ago, I finally read into it, and my suspicions were validated. 

If nothing else, take away these three points from this post:
1. ProtonMail is inherently insecure, if you've used the Webmail client, ProtonMail has always had the ability to grab your password and private encryption key without you knowing, giving them backdated access to your emails. 
2. ProtonMail lies to its supporters and has close ties with intelligence agencies, and world governments.
3. ProtonMail has several points of security failure which can be utilized by many bad actors. 

## HISTORY
There are two versions of the ProtonMail origin story. There's the 'official' one, on their Wikipedia, which describes Proton Technologies as being started by 'a group of scientists from CERN'. 

And then there's the origin story that has been scrubbed from all of ProtonMail's marketing material and denied by official representatives that goes as follows:

The trio who created ProtonMail were CERN researchers along with a MIT graduate. They were [semifinalists](https://www.helpnetsecurity.com/2014/05/22/cern-mit-scientists-launch-swiss-based-secure-webmail/) at the 2014 MIT 100K startup lunch competition.

Why was [his](https://archive.fo/9qmi1) involvement scrubbed from the history of the company? We'll find out later in Part II. 

First let's see how secure ProtonMail really is. 

## CLAIMS
ProtonMail has made the following claims since the early days.

“We have no access to your messages, and since we cannot decrypt them, we cannot share them with third parties,”

There has never been independent verification of these claims until 2018, where [ Nadim Kobeissi released his own analysis](https://eprint.iacr.org/2018/1121.pdf). He responded to the claims made by ProtonMail's technical specification detailing "security features and infrastructure" in July 2016.

Nadim found that ProtonMail's architecture did "not guarantee end to end encryption for the majority of its users" along with a plethora of other concerns. 

The majority of this post is synthesizing [Nadim's technical paper](https://eprint.iacr.org/2018/1121.pdf) into layman language.

 It won't take long to realize how blatantly insecure this is, you don't need to be a cryptographer or computer scientist to understand it. 

Let's start by defining ProtonMail's claims in general security characteristics:

1. Confidentiality: An encrypted email sent from one person to another can only be read by those two people. 
2. Authenticity: An email you received from someone must have been sent by them and can't be spoofed by someone in the middle.

Next, let's understand how ProtonMail's authentication and encryption schemes work.

First, ProtonMail uses a Zero-Knowledge Password Proof to avoid giving anyone else information about your password. 

[ZKPP](https://hackernoon.com/eli5-zero-knowledge-proof-78a276db9eff) has a complex explanation, but its purpose is to show someone you have a valid password without providing them any information (zero knowledge) about its value.

PM uses this method for user authentication, to prevent the user from ever sending ProtonMail their password. Why is this important? 

"The security granted by this protocol extends to the user's private keys, which are encypted with a salted hash of their password before being sent with the server"

Stop right there. Yes, that's right, the most critical piece to the 'private' email service, your private key - is sent to and saved on ProtonMail's server.

PM openly states they have your private key, and it is only a matter of getting access to your password to decrypt the encrypted privacy key. 

In addition to this, ProtonMail has no password requirements, and has been tested with passwords like '1', 'iloveyou', and 'password', which are all trivial to crack in dictionary  attacks. Once these can be confirmed, an attacker has your entire email history. 

That's still not the main flaw: 

THE FLAW 
The inherent security flaw is introduced with the ProtonMail WebMail portal, the normal web application that we've all visited in the browser. 

And the flaw is that it is relatively simple for ProtonMail to serve you a modified version of their web application or the underlying PGP implementation. There is no way to cryptographically verify that you are getting the official version of the web client as [stored in their repository](https://github.com/ProtonMail/WebClient).

If PM decides to act maliciously, they can do so undetected. Unlike the mobile application who's binaries get cryptographically signed to match the official codebase, there is no method to verify a web application. 

Once they have your password, they can use it with the private key that they have stored for you to decrypt any communication you've ever made through ProtonMail. 

Additionally, they can spoof email messages to others on your behalf. 

PM also has a Encrypt-To-Outside feature, which allows you to send encrypted email to other email providers. 

Not only are PM servers involved in this, but a third party, like Microsoft Outlook.

It works by redirecting the recipient to a PM page in which they type a encryption key that they should have previous outside knowledge of, and this key decrypts the message.  They also receive the PM sender's public key so that they can write a reply back. 

This leaves many open attacks: 
1. PM can once again replace the web application or PGP software to recover the original message and passcode. 
2. PM can also give the recipient a different public key, one that they have the private key to, retrieving the reply for themselves, which they can once again reencrypt with the sender's public key - completely undetected. 
3. The third party mail server is free to do the same, sending their own URL, pretending to be PM, allowing them to harvest the encryption key, which allows them to get the original message. Once they have the original message, they can use it to derive the private key. Then they are able to encrypt the reply back to the sender using their public key. 

 CONCLUSIONS & RECOMMENDATIONS
- ProtonMail's WebMail client cannot be verified to do what it says (this goes for most apps, but since private keys are stored on ProtonMail's servers - this is especially true). 
- ProtonMail cannot claim E2E encryption
- A larger implication is that any encrypted web application can't be trusted to encrypt your data. This goes for other mailers and services like CTemplar, CryptPad and more... the research needs to be done. 
- If we really want privacy, users should generate their own public & private keys. 
- [ProtonMail's response to this analysis](https://protonmail.com/blog/cryptographic-architecture-response/) (that tries to brush it aside) shows that they've known about this for a while and aren't going to do anything to fix it

Unfortunately, I wish I had more recommendations but discovering this has got me rethinking every 'encrypted' web application I use on the computer. 

In Part II we will uncover many of ProtonMail's lies and ties to government entities. 

Discuss with the [neo_network](https://t.me/neo_network_chat)
