---
title: Part II - ProtonMail is Inherently Insecure, Your Emails Are Likely Compromised, Ties To Government Agencies
author: Ramiro Romani
date: 2021-02-16
hero: ./images/stamp.jpg
excerpt: Part II of the exposition points out lies to ProtonMail's supporters ties to government agencies  
---

# Part II: ProtonMail is Inherently Insecure, Your Emails Are Likely Compromised, Ties To Government Agencies 🚨🍯🇨🇭
[@neo_network](https://t.me/neo_network)

In Part I, we looked into the details of ProtonMail's security vulnerabilities, and we discovered that IF ProtonMail was a malicious actor, they could easily decrypt all your emails. 

In Part II, we'll see why ProtonMail is very likely a bad actor, after looking at red flags with their origin, blatant lies in their privacy policies, false claims, illegal activities, and more. 

Full disclosure, a lot of the primary research was performed by another investigative journalist, Privacy Watchdog (https://privacy-watchdog.io/) who has been investigating ProtonMail for a while. Rather than retell his findings, I will iterate the main points (there are many), and add commentary on things I have researched on top of it. 

### 1. ProtonMail was likely created under the oversight of US Intelligence Agencies being founded at MIT. Although proved through backdated [articles](https://www.helpnetsecurity.com/2014/05/22/cern-mit-scientists-launch-swiss-based-secure-webmail), [resumes](https://privacy-watchdog.io/wp-content/uploads/2019/09/Wei-Sun-Resume.pdf), and [twitter posts](https://twitter.com/protonmail/status/442044840701079552?lang=en) - ProtonMail denies any involvement with MIT now (look at their Wikipedia, site, etc...). This should strike you as very suspicious.

Here's the full article by [Privacy Watchdog](https://privacy-watchdog.io/protonmails-creation-with-cia-nsa/). 

### 2. ProtonMail flat out lied to its supporters after [raising 550K in crowdfunding](https://www.indiegogo.com/projects/protonmail#/). 
Here's a direct quote from their IndieGogo (crowdfunding) page. 

"We firmly believe that ProtonMail can only succeed in its mission if it remains independent. [...] There are certain powerful governments and corporations out there who are in the business of controlling and exploiting personal data that will try to hinder us."

Only 7 months after the campaign (they may have been in talks during the campaign), ProtonMail sold equity to Charles River's Ventures and FONGIT. 

CRV's founder was part of the US Dept. of State under Obama and delegate to the UN.

FONGIT is financed by the Swiss Government, which has a [MLAT treaty with the US government](https://core.ac.uk/download/pdf/216913277.pdfhttps://www.rhf.admin.ch/dam/data/rhf/strafrecht/rechtsgrundlagen/sr-0-351-933-6-e.pdf), which allows both countries to share user data back and forth. 

Here's more details by [Privacy Watchdog](https://privacy-watchdog.io/protonmails-crowdfunding-equity-sale/).

### 3. Countless lies about its activities , including conducting [illegal cyber warfare](https://www.vice.com/en/article/qvvke7/email-provider-protonmail-says-it-hacked-back-then-walks-claim-back), its privacy policy, deletion policy, and more. 

After looking up the false claims in the [Privacy Watchdog article](https://privacy-watchdog.io/protonmails-false-claims/) it was actually quite astounding to me how easy it is to catch their lies. 

Here's a discussion about the change in [Privacy Policy](https://www.reddit.com/r/ProtonMail/comments/8sxgy0/ip_logging_privacy_policy_update/), in 2018. 
 
In their new privacy policy they say the [following](https://protonmail.com/privacy-policy): 

"IP Logging: By default, ProtonMail does not keep permanent IP logs. However, IP logs may be kept temporarily to combat abuse and fraud, and your IP address may be retained permanently if you are engaged in activities that breach our terms and conditions

Your login IP address is also kept permanently (until you delete it) if you enable authentication logging for your account (by default this is off). The legal basis of this processing is consent, and you are free to opt-in or opt-out at any time in the security panel of your ProtonMail account."

Keep in mind by Swiss law, they are legally required to keep user data around for 6 months. Swiss laws aren't as private as they seem. 

Additionally, they have all your email metadata - this has always been unencrypted and ProtonMail does not offer any protection for this. This includes (from the privacy policy) "sender and recipient email addresses, the IP address incoming messages originated from, message subject, and message sent and received times."

This is incredibly helpful for law enforcement investigations, the content of the emails is usually not needed. 

### 4. ProtonMail fully complies with Swiss authorities, and foreign requests that have been approved by Swiss authorities (MLAT treaty). 

In their warrant canary, they openly show how they retained user data based on a request from the FBI via the MLAT agreement.

They've complied a little under 2,000 times with authorities since [2017](https://protonmail.com/blog/transparency-report/), retaining data, handing over 'encrypted' emails, and the associated metadata (which can be used as evidence just as strong as the content of the emails themselves). 

Conclusion

I hope this puts to bed the notion that ProtonMail can be trusted with your emails. If you're on ProtonMail, I hope you realize as the laws change, that you can and will be compromised. 

Discuss with the [neo_network](https://t.me/neo_network_chat)
