---
title: Saving The World In A Decentralized Manner
author: Ramiro Romani
date: 2021-02-17
hero: ./images/outage.jpg
excerpt: Read about how local organization & mutual aid stepped up to help in a state-wide snow storm & power outage. 
---

# Saving The World In A Decentralized Manner⚡️🥶🆘
[@neo_network](https://t.me/neo_network)

Our beliefs in the current system are being challenged once more as temperatures freeze across the country. 

The [Electric Reliability Council of Texas (ERCOT)](http://www.ercot.com/about) which manages the power generation and distribution to more than 26 million Texas customers (90% of the whole state) is on emergency alert. They have implemented ["rotating outages"](http://www.ercot.com/eea_info/show/26464) which are "controlled, temporary interruptions of electric service. This type of demand reduction is only used as a last resort to preserve the reliability of the electric system as a whole" 

There are currently 2.7M people currently in Texas without power. Check out the live map [here](https://poweroutage.us/area/state/texas).  

At the same time, the food supply chain has not been able to supply large parts of Texas due to hazardous road conditions and power outages. I've had first hand experience seeing cleaned out food shelves since Monday, and now we're hearing local reports of grocery store lines that stretch around the block. 

Who's going to come to our aid? 

We have to come to our own aid. 
And that's exactly how people like Echo Colon have been handling it. 

Echo organizes [Mutual Aid In North Texas.](https://twitter.com/ntxmutualaid/status/1362152407707176961)  which is a mutual aid group in Dallas Fort Worth Area. Mutual aid groups are much like [Freedom Cells](freedomcells.org) in the sense that they're volunteer run, locally based, and bring people together. 

We had the pleasure of hearing some advice from Echo on how people can organize mutual aid groups in their area and help their neighbors. 

"We set up a Facebook page/Instagram/email and we created [2] online survey[s] for people who need things and people who can help. As people fill out the form they’re added to a spreadsheet"

Echo typically uses the Google Forms & Google Sheets combo to do this. I've used Google Sheets in the past myself, its easy to use and robust. 

At this point we should just be aware that our data is not ours when we submit it to cloud services. Interested in finding alternatives,  I researched a few online form services and read through their privacy policies. They all tell you up front that they have full rights to any submission of data. 

However, don't despair. I have also self-hosted my own form applications in the past, which store the submission data on your own server and aren't sending it off to the cloud. 

If you are even a little bit technically proficient, you should look into buying a Virtual Private Server (there are many options in independent providers) and self-hosting your own applications.

You can install entire platforms on your server that allow you to locally install applications from an app store in one-click, like [Yunohost](https://yunohost.org/#/) and [Sandstorm](https://sandstorm.io/). 

If you want to just browse all the software you can self host, here's a whole list of [goodies](https://github.com/awesome-selfhosted/awesome-selfhosted), including document editors, photo storage tools, and of course - surveys. 

If you're only interested in managing survey software, then a few solid options are:
[JDHost](https://www.jdsoft.com/jd-esurvey.html).
[OhMyForm](https://ohmyform.com/docs/install/)
[LimeSurvey](https://www.limesurvey.org/) (harder to use). 

If you're unable to set this stuff up, than don't despair, try and look for someone technically proficient to help you in your community. Voluntary collaboration under a common goal is the essence of Freedom Cells and Mutual Aid Groups. Find individuals to help you with the cause. 

And don't get stuck on the technology, if you are ready to help people - by all means use Google Sheets. Just be aware of where your data could go. (mostly big corporations & world governments). 

Once you have your surveys, up - you can start running the operation. 

"People can then work remotely to connect people who are on the list of needing something to people who have supplies and can help. It takes a bunch of making calls. Also setting up hubs where people can drop off supplies. This can be house, churches, businesses who are down to help. 

So for example we had requests for food for a family of five. We currently have a kitchen set up so we looked on the list of people who could help and grabbed a driver to get food from the kitchen and take it to the family. Or if someone needs a generator, etc.  [...] We also created signal chats that for drivers, one for rapid response, one for general mutual aid info in Austin."

You can see from Echo's advice how much location comes into play. People are able to help others if its in their power to do so. This might be affected by what they have or how far away they live. 

Its a beautiful natural order in the decentralized 'chaos'. No central power is needed to save the world. 

A tool that I think could seriously help this effort is an [open source collaborative mapping tool, FacilMaps](https://facilmap.org/#5/32.789/-96.802/MSfR)  You can keep track of local business that still have food & supplies, drop off points, places with heat / power / water, and more. 

If conditions worsen (all signs point in that direction), then technologies like these will become exceptionally important as we fight to stay informed, connected, and safe. 

And this is not a purely physical or purely digital process. Both parts must work in tandem to save the world. 

The digital side brings in money (through donations from payment providers) and local information (social media, forms), and coordinates the group through messenger apps. 

The in-person side works by being the boots on the ground, building relationships, receiving & offering, helping people in need. 

"during a flood we cooked a bunch of hot food and then took it out on a truck. The truck became a hub that we knew these neighborhoods didn’t have power so that’s where we went. People didn't have internet. So as you go around and feed people who talk to them about what they need and then can bring out those items that day or the next when you have them."

The idea of the digital and the physical being so intimately connected is something I want to explore through the @neo_network - We've always had the options using these technologies for good. Its too often we get lazy and allow centralized powers to use technology for evil. But this is our way to fight back - but all it takes is a little digital & physical effort. 

Lastly, you may feel like you don't have the network set up to do this. 

Echo has this to say: 
"We have been doing this for awhile here in terms of the homeless community so we already had a network kinda in place. But we have also done this very quickly during floods when we didn’t have the network in place."

Thanks for reading, we need to inspire each other to make a greater difference in our communities. Start your local mutual aid group right now. Form a cell on freedomcells.org - love one another. We must turn to each other when the world is coming to an end. 

Discuss with the [neo_network](https://t.me/neo_network_chat)

