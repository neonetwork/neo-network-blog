---
title: Privacy Checkup | A Telegram Setting Can Give People Your Exact Location
author: Ramiro Romani
date: 2021-02-17
hero: ./images/binocs.jpg
excerpt: Not only will it enable random people to message you out of the blue (as the women in my family can attest to), but it can be used to find your exact location. 
---

# Privacy Checkup | A Telegram Setting Can Give People Your Exact Location 🤙🗺🧐
[@neo_network](https://t.me/neo_network)

Didya know @Telegram just became the most downloaded mobile app in the world?

Its pretty crazy to see this kind of growth from a platform. Pavel Durov, one of Telegram's Product Managers attributes their success to a commitment with privacy. 

"You – our users – have been and will always be our only priority. Unlike other popular apps, Telegram doesn’t have shareholders or advertisers to report to. We don’t do deals with marketers, data miners or government agencies. Since the day we launched in August 2013 we haven’t disclosed a single byte of our users' private data to third parties."

Well if that doesn't give me the warm & fuzzies inside. You can take Pavel at his word, but that doesn't stop the huge influx of scammers alongside the new users. 

If you're unaware of your privacy settings, Telegram's feature 'richness' can turn into feature 'risks', kid. (sorry, I had to)

 Let's run through the privacy checkup: 

1. Is your [nearby users setting](https://telegram.org/blog/new-profiles-people-nearby) on? 

Well if it is - turn it off immediately. Not only will it enable random people to message you out of the blue (as the women in my family can attest to), but it can be used to find your exact location. 

First reported in early January by [Ahmed ](https://blog.ahmed.nyc/2021/01/if-you-use-this-feature-on-telegram.html), the exploit works by using Telegram's nearby location feature to find the distance of your target user from a triangle of points. You can either walk around and do the measurements yourself, or use a GPS spoofer to make the process quicker. This is not unlike phone tower triangulation by law enforcement. 

This feature is off by default, but even if you're feeling adventurous, I'd keep it off - you could give away your home address. (and there are never any cuties around anyways) 

2. Are you sharing your [phone number with everyone ](https://telegram.org/faq#q-who-can-see-my-phone-number)? 

Its not uncommon to meet people on Telegram, and after some friendly back and forth you might add them as a contact. But be careful, there's a checkbox allowing you to share your number - essentially giving away your identity through a reverse phone number search. 

Go to your privacy settings and select 'nobody' for 'Who can see my phone number'. Note that if someone out in the world knows your registered phone number already, they're able to find you on Telegram by searching your phone number. 

In the future, I'll share ways to get anonymous & disposable phone numbers 

3. Can anyone add you to their group? 

By default, anyone can invite you to a group, which makes it so you automatically join a group and start receiving messages. We've seen swaths of new crypto scams, fake local marketplace groups, and fake food delivery groups take advantage of this. They have bots (or people) which go in and invite anyone they can to their groups. 

To fix this go into privacy settings and click 'Group Invite' settings, then select 'My contacts' for 'Who can add me to groups and channels'. 

Now, only your Uncle Lester can invite you to his yard sale... 🙄 - and there's no way to turn that off. 

If this even protects one person, I'll be happy I wrote it. 

Stay warm & safe, 

🤙🗺🧐

Discuss with the [neo_network](https://t.me/neo_network_chat)

