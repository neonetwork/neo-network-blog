---
title: Ramiro Romani's Recommended Apps SnackPack Part I
author: Ramiro Romani
date: 2021-02-20
hero: ./images/cheese.jpg
excerpt: These useful apps are all FREE and most things are privacy minded - but not all things. 
---

# Ramiro Romani's Recommended Apps SnackPack February 🍍💠🍎 
[@neo_network](https://t.me/neo_network)

I made a commitment to write for @neo_network every day this month, however I'm feeling lazy today. Here's some low hanging but delicious fruit in the form of app recommendations. 

Everything has a useful FREE version (i love free) and most things are privacy minded - but not all things. Do your own research (and then share it with us on @neo_network_chat) 

Productivity
**CryptPad** (https://cryptpad.fr/): Browser-based cloud Encrypted Office Suite With Storage, Word Documents, Spreadsheets , Presentations & More 🔥
**CherryTree** (https://www.giuspen.com/cherrytree/): A Personal (Local) Hierarchical Note Taking System with rich text formatting, code blocks, links, and more. I use it like a personal wikipedia or for journals. Your CherryTree notebooks can be saved as an encrypted database file. 🍒
**Obsidian** (https://obsidian.md/): A Graph-Based Knowledge Base & Markdown Editor: I usually compose my more serious writing here. 💜
**YunoHost** (http://yunohost.org/): Self-hosting platform for email, calendar, web applications & more 📆 

Communication
**Jitsi Meet** (http://meet.jit.si/): Open Source & Private Real-Time Video Conferencing With Screen Sharing, Chat & More 🧞‍♂️
**CTemplar** (https://ctemplar.com/): Highly Secure Email Service Based In Iceland 🧊I've written a bit about it at @neo_network

Technical:
**FileZilla** (https://filezilla-project.org/): A Really Easy Way To Transfers Files Quickly Between Any Computers within the same Network 📂
**VSCodium** (https://github.com/VSCodium/vscodium): The Truly Open-Souce Fork of VSCode (now with less telemetry!) 👩‍💻

Crypto:
**Atomic Wallet** (https://atomicwallet.io/downloads): The Cross-Platform Wallet I've Seen With The Most Supported Tokens and Built-in Exchange (pretty trustworthy, except I can't get my monero out of there, damn it) ⚛️
**CryptoWatch** (https://cryptowat.ch/): Awesome Crypto-Trading Dashboard to watch coin prices & see candlestick charts 💎

Till tomorrow then, :) 

Discuss with the [neo_network](https://t.me/neo_network_chat)

