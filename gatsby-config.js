module.exports = {
  pathPrefix: ``, // served from root
  siteMetadata: {
    title: `neo-network`,
    name: `Ramiro Romani's neo-network`,
    siteUrl: `https://neonetwork.in`,
    description: `
    neo: a new or revived form of.
    
    Break free of the former internet. Daily posts & content that teach you new revolutionary technologies.`,
    hero: {
      heading: `Break free of the former internet. Join the neo-network.`,
      maxWidth: 900,
    },
    social: [
      {
        name: `twitter`,
        url: `https://twitter.com/narative`,
      },
      {
        name: `github`,
        url: `https://github.com/narative`,
      },
      {
        name: `instagram`,
        url: `https://instagram.com/narative.co`,
      },
      {
        name: `linkedin`,
        url: `https://www.linkedin.com/company/narative/`,
      },
      {
        name: `dribbble`,
        url: `https://dribbble.com/narativestudio`,
      },
    ],
  },
  plugins: [
    'gatsby-plugin-theme-ui',
    'gatsby-plugin-breakpoints',
    {
      resolve: "@narative/gatsby-theme-novela",
      options: {
        contentPosts: "content/posts",
        contentAuthors: "content/authors",
        basePath: "/",
        authorsPage: true,
        sources: {
          local: true,
          // contentful: true,
        },
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Novela by Narative`,
        short_name: `Novela`,
        start_url: `/`,
        background_color: `#fff`,
        theme_color: `#fff`,
        display: `standalone`,
        icon: `src/assets/favicon.png`,
      },
    },
    {
      resolve: `gatsby-plugin-netlify-cms`,
      options: {
      },
    },
  ],
};
