import React from 'react';
import { useBreakpoint } from 'gatsby-plugin-breakpoints';
/**
 * Paste in your SVG logo and return it from this component.
 * Make sure you have a height set for your logo.
 * It is recommended to keep the height within 25-35px.
 * Logo comes with a property value called `fill`. `fill` is useful 
 * when you want to change your logo depending on the theme you are on. 
 */
export default function Logo({ fill }) {
    let lightMode = (fill === `#000`);
    const breakpoints = useBreakpoint();

    let logo;
    if (breakpoints.sm) return (lightMode) ? SmallBlackLogo : SmallWhiteLogo;
    
  return (
    (lightMode) ? FullLogoBlack : FullLogoWhite
  );
}

const FullLogoWhite = (
<svg
   height="50"
   viewBox="0 0 613.88477 98.879425"
  
    >
<g
   id="g50"
   transform="translate(-236.01,-490.56029)">
	<g
   id="g22">
		
			<polyline
   fill="none"
   stroke="#ffffff"
   stroke-width="9.446"
   stroke-linecap="round"
   stroke-linejoin="round"
   stroke-miterlimit="10"
   points="    762.275,562.359 762.275,517.642 801.002,584.718 801.002,495.282 839.729,562.359 839.729,517.642   "
   id="polyline2" />
		<g
   id="g8">
			
				<circle
   fill="none"
   stroke="#ffffff"
   stroke-width="11.8075"
   stroke-linecap="round"
   stroke-linejoin="round"
   stroke-miterlimit="10"
   cx="762.276"
   cy="562.35901"
   r="4.2610002"
   id="circle4" />
			<circle
   cx="762.276"
   cy="562.35901"
   r="4.2610002"
   id="circle6" />
		</g>
		<g
   id="g14">
			
				<circle
   fill="none"
   stroke="#ffffff"
   stroke-width="11.8075"
   stroke-linecap="round"
   stroke-linejoin="round"
   stroke-miterlimit="10"
   cx="801.00201"
   cy="540"
   r="4.2600002"
   id="circle10" />
			<circle
   cx="801.00201"
   cy="540"
   r="4.2600002"
   id="circle12" />
		</g>
		<g
   id="g20">
			
				<circle
   fill="none"
   stroke="#ffffff"
   stroke-width="11.8075"
   stroke-linecap="round"
   stroke-linejoin="round"
   stroke-miterlimit="10"
   cx="839.729"
   cy="517.72998"
   r="4.2620001"
   id="circle16" />
			<circle
   cx="839.729"
   cy="517.72998"
   r="4.2620001"
   id="circle18" />
		</g>
	</g>
	<g
   id="g28">
		
			<circle
   fill="none"
   stroke="#ffffff"
   stroke-width="11.8075"
   stroke-linecap="round"
   stroke-linejoin="round"
   stroke-miterlimit="10"
   cx="390.30399"
   cy="540"
   r="4.2600002"
   id="circle24" />
		<circle
   cx="390.30399"
   cy="540"
   r="4.2600002"
   id="circle26" />
	</g>
	<path
   fill="#ffffff"
   d="m 277.773,514.798 v 50.405 H 266.252 L 250.41,542.16 v 23.043 h -14.4 v -50.405 h 11.521 l 15.842,23.042 v -23.042 z"
   id="path30" />
	<path
   fill="#ffffff"
   d="m 318.093,551.953 v 13.25 H 284.97 v -50.405 h 32.763 v 13.105 h -18.361 v 5.473 h 16.562 v 12.961 h -16.562 v 5.616 z"
   id="path32" />
	<path
   fill="#ffffff"
   d="m 321.69,540 c 0,-14.904 11.809,-26.354 26.714,-26.354 14.905,0 26.715,11.45 26.715,26.354 0,14.906 -11.81,26.354 -26.715,26.354 -14.905,0 -26.714,-11.448 -26.714,-26.354 z m 39.028,0 c 0,-6.984 -5.185,-12.24 -12.313,-12.24 -7.128,0 -12.313,5.256 -12.313,12.24 0,6.984 5.185,12.242 12.313,12.242 7.128,0 12.313,-5.258 12.313,-12.242 z"
   id="path34" />
	<path
   fill="#ffffff"
   d="m 447.277,514.798 v 50.405 H 435.756 L 419.915,542.16 v 23.043 h -14.401 v -50.405 h 11.521 l 15.842,23.042 v -23.042 z"
   id="path36" />
	<path
   fill="#ffffff"
   d="m 487.596,551.953 v 13.25 h -33.123 v -50.405 h 32.763 v 13.105 h -18.361 v 5.473 h 16.562 v 12.961 h -16.562 v 5.616 z"
   id="path38" />
	<path
   fill="#ffffff"
   d="m 528.636,528.768 h -11.882 v 36.436 h -14.4 v -36.436 h -11.881 v -13.97 h 38.163 z"
   id="path40" />
	<path
   fill="#ffffff"
   d="m 529.71,514.798 h 15.843 l 6.84,27.723 8.137,-27.723 h 11.088 l 8.137,27.723 6.842,-27.723 h 15.842 l -14.258,50.405 h -14.186 l -7.92,-26.931 -7.922,26.931 h -14.186 z"
   id="path42" />
	<path
   fill="#ffffff"
   d="m 601.352,540 c 0,-14.904 11.809,-26.354 26.715,-26.354 14.904,0 26.713,11.45 26.713,26.354 0,14.906 -11.809,26.354 -26.713,26.354 -14.907,0 -26.715,-11.448 -26.715,-26.354 z m 39.027,0 c 0,-6.984 -5.184,-12.24 -12.313,-12.24 -7.129,0 -12.314,5.256 -12.314,12.24 0,6.984 5.186,12.242 12.314,12.242 7.128,0 12.313,-5.258 12.313,-12.242 z"
   id="path44" />
	<path
   fill="#ffffff"
   d="m 678.176,549.289 h -3.6 v 15.842 h -14.4 v -50.405 h 20.16 c 10.873,0 19.082,6.841 19.082,17.714 0,6.121 -3.023,11.018 -7.775,13.897 l 9.936,18.794 H 686.17 Z m -3.6,-11.953 h 5.473 c 3.023,0.072 4.969,-1.656 4.969,-4.607 0,-2.953 -1.945,-4.753 -4.969,-4.753 h -5.473 z"
   id="path46" />
	<path
   fill="#ffffff"
   d="M 732.105,565.203 719.576,543.6 v 21.603 h -14.4 v -50.405 h 14.4 v 20.162 l 11.809,-20.162 h 15.986 l -14.979,24.482 15.697,25.923 z"
   id="path48" />
</g>
</svg>

);

const FullLogoBlack = (    <svg

    xmlns="http://www.w3.org/2000/svg"
    version="1.1"
    id="design"
    x="0px"
    y="0px"
    height="50"
    viewBox="0 0 613.88477 98.879425"

>
 <g
    id="g964"
    transform="translate(-236.01,-490.56029)"><polyline
      fill="none"
      stroke="#160917"
      stroke-width="9.446"
      stroke-linecap="round"
      stroke-linejoin="round"
      stroke-miterlimit="10"
      points="    762.275,562.359 762.275,517.642 801.002,584.718 801.002,495.282 839.729,562.359 839.729,517.642   "
      id="polyline2"
       /><circle
      fill="none"
      stroke="#160917"
      stroke-width="11.8075"
      stroke-linecap="round"
      stroke-linejoin="round"
      stroke-miterlimit="10"
      cx="762.276"
      cy="562.35901"
      r="4.2610002"
      id="circle4"
       /><circle
      fill="#ffffff"
      cx="762.276"
      cy="562.35901"
      r="4.2610002"
      id="circle6" /><g
      id="g14"
      >
             
                 <circle
    fill="none"
    stroke="#160917"
    stroke-width="11.8075"
    stroke-linecap="round"
    stroke-linejoin="round"
    stroke-miterlimit="10"
    cx="801.00201"
    cy="540"
    r="4.2600002"
    id="circle10"
     />
             <circle
    fill="#ffffff"
    cx="801.00201"
    cy="540"
    r="4.2600002"
    id="circle12"
     />
         </g><g
      id="g20"
      >
             
                 <circle
    fill="none"
    stroke="#160917"
    stroke-width="11.8075"
    stroke-linecap="round"
    stroke-linejoin="round"
    stroke-miterlimit="10"
    cx="839.729"
    cy="517.72998"
    r="4.2620001"
    id="circle16"
     />
             <circle
    fill="#ffffff"
    cx="839.729"
    cy="517.72998"
    r="4.2620001"
    id="circle18"
     />
         </g><g
      id="g937"><g
        id="g28">
         
             <circle
    fill="none"
    stroke="#160917"
    stroke-width="11.8075"
    stroke-linecap="round"
    stroke-linejoin="round"
    stroke-miterlimit="10"
    cx="390.30499"
    cy="540"
    r="4.2600002"
    id="circle24" />
         <circle
    fill="#ffffff"
    cx="390.30499"
    cy="540"
    r="4.2600002"
    id="circle26" />
     </g><path
        d="m 277.774,514.798 v 50.405 H 266.253 L 250.412,542.16 v 23.043 H 236.01 v -50.405 h 11.521 l 15.842,23.042 v -23.042 z"
        id="path30"
         /><path
        d="m 318.093,551.953 v 13.25 H 284.97 v -50.405 h 32.763 v 13.105 h -18.362 v 5.473 h 16.562 v 12.961 h -16.562 v 5.616 z"
        id="path32"
         /><path
        d="m 321.69,540 c 0,-14.904 11.809,-26.354 26.714,-26.354 14.905,0 26.714,11.45 26.714,26.354 0,14.906 -11.809,26.354 -26.714,26.354 -14.905,0 -26.714,-11.448 -26.714,-26.354 z m 39.028,0 c 0,-6.984 -5.185,-12.24 -12.313,-12.24 -7.129,0 -12.313,5.256 -12.313,12.24 0,6.984 5.185,12.242 12.313,12.242 7.128,0 12.313,-5.258 12.313,-12.242 z"
        id="path34"
         /><path
        d="m 447.277,514.798 v 50.405 H 435.756 L 419.915,542.16 v 23.043 h -14.401 v -50.405 h 11.521 l 15.842,23.042 v -23.042 z"
        id="path36"
         /><path
        d="m 487.596,551.953 v 13.25 h -33.123 v -50.405 h 32.763 v 13.105 h -18.362 v 5.473 h 16.562 v 12.961 h -16.562 v 5.616 z"
        id="path38"
         /><path
        d="m 528.636,528.768 h -11.882 v 36.436 h -14.4 v -36.436 h -11.881 v -13.97 h 38.164 v 13.97 z"
        id="path40"
         /><path
        d="m 529.71,514.798 h 15.842 l 6.84,27.723 8.137,-27.723 h 11.088 l 8.137,27.723 6.842,-27.723 h 15.842 l -14.258,50.405 h -14.186 l -7.92,-26.931 -7.922,26.931 h -14.186 z"
        id="path42"
         /><path
        d="m 601.352,540 c 0,-14.904 11.809,-26.354 26.715,-26.354 14.904,0 26.713,11.45 26.713,26.354 0,14.906 -11.809,26.354 -26.713,26.354 -14.907,0 -26.715,-11.448 -26.715,-26.354 z m 39.027,0 c 0,-6.984 -5.184,-12.24 -12.313,-12.24 -7.129,0 -12.314,5.256 -12.314,12.24 0,6.984 5.186,12.242 12.314,12.242 7.128,0 12.313,-5.258 12.313,-12.242 z"
        id="path44"
         /><path
        d="m 678.176,549.289 h -3.6 v 15.842 h -14.4 v -50.405 h 20.16 c 10.873,0 19.082,6.841 19.082,17.714 0,6.121 -3.023,11.018 -7.775,13.897 l 9.936,18.794 H 686.17 Z m -3.6,-11.953 h 5.473 c 3.023,0.072 4.969,-1.656 4.969,-4.607 0,-2.953 -1.945,-4.753 -4.969,-4.753 h -5.473 z"
        id="path46"
         /><path
        d="M 732.105,565.203 719.576,543.6 v 21.603 h -14.4 v -50.405 h 14.4 v 20.162 l 11.809,-20.162 h 15.986 l -14.979,24.482 15.697,25.923 z"
        id="path48"
         /></g></g>
 </svg>)

const SmallBlackLogo = (
    <svg
   height="50"
   viewBox="0 0 328.00031 331.67725"
   enable-background="new 0 0 1080 1080"
>
<g
   id="g22"
   transform="translate(-375.99935,-374.16137)">
	
		<polyline
   fill="none"
   stroke="#000000"
   stroke-width="31.6859"
   stroke-linecap="round"
   stroke-linejoin="round"
   stroke-miterlimit="10"
   points="   410.094,615 410.094,465 539.999,690 539.999,390 669.906,615 669.906,465  "
   id="polyline2" />
	<g
   id="g8">
		
			<circle
   fill="none"
   stroke="#000000"
   stroke-width="39.6073"
   stroke-linecap="round"
   stroke-linejoin="round"
   stroke-miterlimit="10"
   cx="410.095"
   cy="615"
   r="14.292"
   id="circle4" />
		<circle
   fill="#ffffff"
   cx="410.095"
   cy="615"
   r="14.292"
   id="circle6" />
	</g>
	<g
   id="g14">
		
			<circle
   fill="none"
   stroke="#000000"
   stroke-width="39.6073"
   stroke-linecap="round"
   stroke-linejoin="round"
   stroke-miterlimit="10"
   cx="539.99902"
   cy="540"
   r="14.292"
   id="circle10" />
		<circle
   fill="#ffffff"
   cx="539.99902"
   cy="540"
   r="14.292"
   id="circle12" />
	</g>
	<g
   id="g20">
		
			<path
   fill="none"
   stroke="#000000"
   stroke-width="39.6073"
   stroke-linecap="round"
   stroke-linejoin="round"
   stroke-miterlimit="10"
   d="m 684.196,465.295 c 0,7.896 -6.396,14.292 -14.29,14.292 -7.894,0 -14.295,-6.396 -14.295,-14.292 0,-7.89 6.401,-14.291 14.295,-14.291 7.894,0 14.29,6.401 14.29,14.291 z"
   id="path16" />
		<path
   fill="#ffffff"
   d="m 684.196,465.295 c 0,7.896 -6.396,14.292 -14.29,14.292 -7.894,0 -14.295,-6.396 -14.295,-14.292 0,-7.89 6.401,-14.291 14.295,-14.291 7.894,0 14.29,6.401 14.29,14.291 z"
   id="path18" />
	</g>
</g>
</svg>

);
 const SmallWhiteLogo = (
    <svg
    height="50"
    viewBox="0 0 328 331.67505"
    enable-background="new 0 0 1080 1080">
 <g
    id="g22"
    transform="translate(-375.9995,-374.16247)">
     
         <polyline
    fill="none"
    stroke="#ffffff"
    stroke-width="31.6857"
    stroke-linecap="round"
    stroke-linejoin="round"
    stroke-miterlimit="10"
    points="   410.094,615 410.094,465 539.999,689.999 539.999,390.001 669.904,615 669.904,465  "
    id="polyline2" />
     <g
    id="g8">
         
             <circle
    fill="none"
    stroke="#ffffff"
    stroke-width="39.607"
    stroke-linecap="round"
    stroke-linejoin="round"
    stroke-miterlimit="10"
    cx="410.095"
    cy="615"
    r="14.292"
    id="circle4" />
         <circle
    cx="410.095"
    cy="615"
    r="14.292"
    id="circle6" />
     </g>
     <g
    id="g14">
         
             <circle
    fill="none"
    stroke="#ffffff"
    stroke-width="39.607"
    stroke-linecap="round"
    stroke-linejoin="round"
    stroke-miterlimit="10"
    cx="539.99799"
    cy="540"
    r="14.292"
    id="circle10" />
         <circle
    cx="539.99799"
    cy="540"
    r="14.292"
    id="circle12" />
     </g>
     <g
    id="g20">
         
             <path
    fill="none"
    stroke="#ffffff"
    stroke-width="39.607"
    stroke-linecap="round"
    stroke-linejoin="round"
    stroke-miterlimit="10"
    d="m 684.196,465.295 c 0,7.896 -6.399,14.293 -14.292,14.293 -7.893,0 -14.292,-6.397 -14.292,-14.293 0,-7.89 6.398,-14.29 14.292,-14.29 7.894,0 14.292,6.4 14.292,14.29 z"
    id="path16" />
         <path
    d="m 684.196,465.295 c 0,7.896 -6.399,14.293 -14.292,14.293 -7.893,0 -14.292,-6.397 -14.292,-14.293 0,-7.89 6.398,-14.29 14.292,-14.29 7.894,0 14.292,6.4 14.292,14.29 z"
    id="path18" />
     </g>
 </g>
 </svg>
)