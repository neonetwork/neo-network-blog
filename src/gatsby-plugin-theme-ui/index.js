import novelaTheme from '@narative/gatsby-theme-novela/src/gatsby-plugin-theme-ui';
export default {
  ...novelaTheme,
  initialColorMode: `dark`,
  colors: {
      ...novelaTheme.colors,    
      modes: {
          light: {
            ...novelaTheme.colors.modes.light,
            background: '#F8F2DC',
            primary: '#160917',
            secondary: '#EF075B',
            highlight: '#EF075B',
            accent: '#F18805',
            grey: '#3B0217',
            gradient: ""
          },
          dark: {
            ...novelaTheme.colors.modes.dark,
            gradient: "",
            primary: '#F8F2DC',
            secondary: '#EF075B',
            highlight: '#EF075B',
            accent: '#F18805',
            grey: '#F1EDE3',
            background: '#160917',
          }
      },
      fonts: {
        heading: 'Priborg'
      }

  }
};